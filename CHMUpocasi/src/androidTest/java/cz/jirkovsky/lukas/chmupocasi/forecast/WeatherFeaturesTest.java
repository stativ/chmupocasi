/*
 *   Copyright 2014-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast;

import junit.framework.TestCase;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.WeatherFeatures;
import cz.jirkovsky.lukas.chmupocasi.forecast.parsing.WeatherFeaturesParser;

/**
 * Tests for the WeatherFeatures.
 */
public class WeatherFeaturesTest extends TestCase {
    /// machine epsilon used for floating point number comparison
    @SuppressWarnings("FieldCanBeLocal")
    private static double EPS = 0.0001;

    /**
     * Tests for crashes caused by unhandled exceptions
     */
    @SuppressWarnings("UnusedAssignment")
    public void testExceptions() {
        WeatherFeatures features = null;

        // incomplete text
        features = WeatherFeaturesParser.parse("míst");
        // issue #5 (a keyword is the last word in the forecast)
        features = WeatherFeaturesParser.parse("místy");
    }

    /**
     * Test for parser itself
     */
    public void testParsing() {
        WeatherFeatures features;

        features = WeatherFeaturesParser.parse("Skoro jasno.");
        assertEquals(0.1875, features.getCloudCover(), EPS);

        features = WeatherFeaturesParser.parse("Oblačno až zataženo, místy déšť nebo přeháňky. Odpoledne od severozápadu ustávání srážek a ubývání oblačnosti.");
        assertEquals(0.7182421875, features.getCloudCover(), EPS);
        assertEquals(0.298, features.getRain(), EPS);
        assertEquals(0.298, features.getShowers(), EPS);
        assertEquals(0.0, features.getStorm(), EPS);
        assertEquals(0.0, features.getSnow(), EPS);
    }
}

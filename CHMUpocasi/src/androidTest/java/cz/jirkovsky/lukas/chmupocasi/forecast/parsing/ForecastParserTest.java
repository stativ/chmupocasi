/*
 *   Copyright 2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.parsing;

import junit.framework.TestCase;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.DayForecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Days;

/**
 * Tests of the forecast parser.
 */
public class ForecastParserTest extends TestCase {
    public void test1() throws IOException {
        InputStream teststream = getClass().getResourceAsStream("test1.html");
        assertNotNull(teststream);

        ArrayList<DayForecast> forecast = ForecastParser.parse(teststream);
        assertEquals(5, forecast.size());
        assertEquals(Days.NIGHT, forecast.get(0).getMatchingDay());
        assertEquals(Days.Fri, forecast.get(1).getMatchingDay());
        assertEquals(Days.Sat, forecast.get(2).getMatchingDay());
        assertEquals(Days.Sun, forecast.get(3).getMatchingDay());
        assertEquals(Days.Mon, forecast.get(4).getMatchingDay());
    }
}

#!/bin/sh

SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )
RESPATH=$SCRIPTPATH/../res

cd $SCRIPTPATH

for INFILE in ic_weather_*.svg ; do
	OUTFILE="${INFILE%.svg}.png"

	inkscape --export-area-page -h 48 -o $RESPATH/drawable-mdpi/$OUTFILE $INFILE
	inkscape --export-area-page -h 72 -o $RESPATH/drawable-hdpi/$OUTFILE $INFILE
	inkscape --export-area-page -h 96 -o $RESPATH/drawable-xhdpi/$OUTFILE $INFILE
done

# special handling of the snow icon
mogrify -trim $RESPATH/drawable-*/ic_weather_snow.png

# optimize all files
optipng -o 7 $RESPATH/drawable-*/ic_weather_*.png

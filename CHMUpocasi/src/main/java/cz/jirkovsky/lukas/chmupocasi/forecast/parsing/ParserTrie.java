/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A modification of Trie that is used to parser weather.
 *
 * The trie is modified to use hash maps of the first letter after the prefix
 * to increase access speed.
 *
 * @param <State> state instate to use for the parser execution
 */
class ParserTrie<State> {
    /**
     * Parser runner interface.
     *
     * @param <State> state instate to use for the parser execution
     */
    public interface FeatureParser<State> {
        /**
         * The parser callback function.
         *
         * @param state the user state
         */
        void run(State state);
    }

    private static class Node<State> {
        Map<Character, List<Node<State>>> map = new HashMap<>();
        String string;
        FeatureParser<State> parser = null;

        private Node() {}

        private Node(String string, FeatureParser<State> parser) {
            this.string = string;
            this.parser = parser;
        }
    }

    public static class Value<State> {
        FeatureParser<State> parser;
        int end;

        public Value(FeatureParser<State> parser, int end) {
            this.parser = parser;
            this.end = end;
        }
    }

    private Node<State> root = new Node<>();

    public void put(String keyword, FeatureParser<State> parser) {
        int pos = 0;
        Node<State> node = root;
        List<Node<State>> nodeList;

        while (true) {
            Character c = keyword.charAt(pos++);
            if ((nodeList = node.map.get(c)) == null) {
                List<Node<State>> newList = new ArrayList<>();
                newList.add(new Node<>(keyword.substring(pos), parser));
                node.map.put(c, newList);

                return;
            }
            else {
                // if there are some nodes starting with the same letter, try to find the best matching one
                int longestPrefix = 0;
                int bestNodePos = -1;
                for (int it = 0; it < nodeList.size(); ++it) {
                    int prefixLength = longestCommonPrefix(keyword, pos, nodeList.get(it).string);
                    if (prefixLength > longestPrefix) {
                        longestPrefix = prefixLength;
                        bestNodePos = it;
                    }
                }

                // if there is no other node that has the same prefix, add a new node to the list
                if (longestPrefix == 0) {
                    nodeList.add(new Node<>(keyword.substring(pos), parser));

                    return;
                }

                // in case the longest prefix is the same as the best matching node content, we will repeat the insertion code
                Node<State> bestNode = nodeList.get(bestNodePos);
                if (longestPrefix == bestNode.string.length()) {
                    node = bestNode;
                    pos += longestPrefix;
                    continue;
                }

                // now if there is some node with the same prefix, we have to insert a new code with the common
                // prefix and move the rest down the tree
                Node<State> prefixNode = new Node<>(bestNode.string.substring(0, longestPrefix), null);
                // for the new node omit the first character, because it will be used in the map
                Node<State> newNode = new Node<>(keyword.substring(pos + longestPrefix + 1), parser);

                List<Node<State>> bestList = new ArrayList<>();
                List<Node<State>> newList = new ArrayList<>();
                bestList.add(bestNode);
                newList.add(newNode);
                prefixNode.map.put(bestNode.string.charAt(longestPrefix), bestList);
                prefixNode.map.put(keyword.charAt(pos + longestPrefix), newList);

                // omit the prefix and one character (used in map) from the best node
                bestNode.string = bestNode.string.substring(longestPrefix + 1);

                // finally replace the bestNode by the prefixNode in the nodeList
                nodeList.set(bestNodePos, prefixNode);

                return;
            }
        }
    }

    public Value<State> get(String string, int startPos) {
        Node<State> processedNode = root;
        Node<State> retNode = root;
        final int strLen = string.length();
        int pos = startPos;
        int tmpPos;
        List<Node<State>> nodeList = new ArrayList<>();

        outer:
        while (pos < strLen) {
            nodeList.clear();
            List<Node<State>> nodeList1 = processedNode.map.get(Character.toLowerCase(string.charAt(pos)));
            // handle wildcard at the beginning, too
            List<Node<State>> nodeList2 = processedNode.map.get('.');

            if (nodeList1 == null && nodeList2 == null) {
                break;
            }
            // create a list encompassing elements from both lists
            if (nodeList1 != null) {
                nodeList.addAll(nodeList1);
            }
            if (nodeList2 != null) {
                nodeList.addAll(nodeList2);
            }

            for (Node<State> node : nodeList) {
                if ( (tmpPos = startsWith(string, pos+1, node.string)) > 0 ) {
                    processedNode = node;
                    pos = tmpPos;
                    if (processedNode.parser != null) {
                        retNode = processedNode;
                    }
                    continue outer;
                }
            }
            break;
        }

        return retNode.parser == null ? null : new Value<>(retNode.parser, pos);
    }

    private int longestCommonPrefix(String string, int startPos, String mapContent) {
        int prefixLength = 0;

        int lim = Math.min(string.length() - startPos, mapContent.length());
        for (int i = lim; i != 0; --i) {
            if (string.charAt(startPos++) != mapContent.charAt(prefixLength)) {
                break;
            }
            ++prefixLength;
        }

        return prefixLength;
    }

    // returns true if string starts with pattern
    private int startsWith(String string, int pos, String pattern) {
        try {
            final int len = pattern.length();
            for (int i = 0; i < len; ++i) {
                if (string.charAt(pos++) != pattern.charAt(i)) {
                    // one character
                    if (pattern.charAt(i) == '.') {
                        continue;
                    }
                    // zero ore one character
                    if (pattern.charAt(i) == '?') {
                        // it's the last character, so it's OK
                        if (i == len - 1) {
                            return 0;
                        }
                        // otherwise we need to check the next char
                        if (string.charAt(pos - 1) == pattern.charAt(i + 1)) {
                            --pos;
                            continue;
                        }
                        if (string.charAt(pos) == pattern.charAt(i + 1)) {
                            continue;
                        }
                    }
                    return 0;
                }
            }
        }
        // catches the case when the pattern is longer than the string
        catch (StringIndexOutOfBoundsException e) {
            return 0;
        }

        return pos;
    }
}

/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi;

import android.app.Fragment;
import android.os.Bundle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import cz.jirkovsky.lukas.chmupocasi.forecast.ForecastCache;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * A fragment that is used to retain the cache across configuration changes.
 */
// NOTE: The class MUST BE PUBLIC, otherwise the Fragment will crash when being instantiated
//       after low-mem conditions with the following exception:
// android.app.Fragment$InstantiationException: Unable to instantiate fragment cz.jirkovsky.lukas.chmupocasi.RetainFragment:
// make sure class name exists, is public, and has an empty constructor that is public
@SuppressWarnings("WeakerAccess")
public class RetainFragment extends Fragment {
    private ForecastCache cache;
    private HashMap<Region, MessageData> waitingMessages;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // create cache
        if (savedInstanceState != null) {
            cache = (ForecastCache) savedInstanceState.getSerializable("cache");
            if (cache == null) {
                initCache();
            }
        } else {
            initCache();
        }
        // waiting messages are not that important, so we can create empty map even after lowmem condition
        waitingMessages = new HashMap<>();
        // retain the fragment across the configuration changes
        setRetainInstance(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("cache", cache);
    }

    @Override
    public void onStop() {
        super.onStop();
        // try to store the cache to the app cache storage
        writeCache();
    }

    public ForecastCache getCache() {
        return cache;
    }

    public HashMap<Region, MessageData> getWaitingMessages() {
        return waitingMessages;
    }

    private void initCache() {
        cache = readCache();
        if (cache == null) {
            cache = new ForecastCache();
        }
    }

    private ForecastCache readCache() {
        ForecastCache retVal = null;
        File cacheFile = new File(getActivity().getCacheDir(), "cache.dat");
        try {
            FileInputStream fis = new FileInputStream(cacheFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            retVal = (ForecastCache) ois.readObject();
            ois.close();
        } catch (Exception e) {
            // do nothing, not essential for the correct working of application
        }
        return retVal;
    }

    private void writeCache() {
        File cacheFile = new File(getActivity().getCacheDir(), "cache.dat");
        try {
            if ((cacheFile.exists() && cacheFile.canWrite()) || cacheFile.createNewFile()) {
                FileOutputStream fos = new FileOutputStream(cacheFile);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(cache);
                oos.close();
            }
        } catch (IOException e) {
            // don't do anything, as it's not crucial if this fails
        }
    }
}

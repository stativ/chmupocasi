/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.Forecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * Caches the downloaded forecast for each location (kraj).
 *
 * The cache also handles "freshness" for each entry, which is used to
 * determine whether the forecast is still valid and how old it is.
 */
public class ForecastCache implements Serializable {
    private static class CacheEntry implements Serializable {
        Date entryTime;
        Forecast forecast;
    }
    public enum Freshness {
        FRESH,
        RECENT,
        OLD
    }

    // All times have two hours offset against actual common update times
    // as the forecast is usually not published immediately.
    // That means that instead of 11 this array will contain 13
    private static final int[] updateTimes = {8, 13, 19};
    // number of hours until the change in Freshness
    private static final int[][] outdateHours = {
            // the first entry is zero, so we can index by Freshness value immediately
            // the second entry is when the forecast becomes recent, which is when new forecast is published
            // the third when it becomes old, which is in the evening (19.00) day before it goes outdated
            {0, 5, 35}, // for 8
            {0, 5, 59}, // for 13
            {0,13, 48}, // for 19
    };
    // avoid requesting the time zone every time we need it
    private static final TimeZone pragueTimeZone = TimeZone.getTimeZone("Europe/Prague");
    // number of milliseconds in hour
    private static final long hourMillis = 60L * 60L * 1000L;

    private final CacheEntry[] cache = new CacheEntry[Region.values().length];

    /**
     * Returns how old the forecast is.
     *
     * The returned value takes the common times when the forecast is presented
     * (ie. 5.45, 11.00, 17.00) into account.
     *
     * As a side effect, this method also removes the old forecast from the specified region's entry.
     *
     * possible return values are:
     * <ul>
     *  <li>FRESH - the cache is up-to-date and doesn't need to be updates</li>
     *  <li>RECENT - the cache is less than a day old, so it's up-to-date, but there may be some update,
     *          used eg. when the last forecast was downloaded in the morning, but it's afternoon now</li>
     *  <li>OLD - the cache is old, but there still may be some useful forecast. If the cache is completely
     *            outdated, the getEntry will return null for such entry. It is also returned if there's
     *            no cache for the specified region.</li>
     * </ul>
     *
     * @param region region for which we want to examine it's up-to-dateness
     * @return how up-to date the cache is, as described above.
     */
    public Freshness getFreshness(Region region) {
        CacheEntry entry = cache[region.ordinal()];
        if (entry == null) {
            return Freshness.OLD;
        }

        int diff = hourDiff(entry.entryTime);
        int updateTime = getUpdateTimeIndex(entry.entryTime);

        if (diff < outdateHours[updateTime][Freshness.RECENT.ordinal()]) {
            return Freshness.FRESH;
        }

        // if the forecast is not completely fresh, we also run the cleanup routine
        entry.forecast.cleanupForecast();
        if (entry.forecast.getForecast().isEmpty()) {
            // clean the cache
            cache[region.ordinal()] = null;
            return Freshness.OLD;
        }

        if (diff < outdateHours[updateTime][Freshness.OLD.ordinal()]) {
            return Freshness.RECENT;
        }

        return Freshness.OLD;
    }

    public void addEntry(Region region, Forecast forecast) {
        int i = region.ordinal();
        if (cache[i] == null) {
            cache[i] = new CacheEntry();
        }

        cache[i].entryTime = getUpdateDate();
        cache[i].forecast = forecast;
    }

    /** Get the forecast in a specified region.
     *
     * The forecast is cleaned up of the entries older than one day.
     *
     * @param region region
     * @return forecast or null if there is no cached entry
     */
    public Forecast getEntry(Region region) {
        CacheEntry entry = cache[region.ordinal()];
        if (entry == null) {
            return null;
        }
        else {
            return entry.forecast;
        }
    }

    private Date getUpdateDate() {
        Calendar calendar = Calendar.getInstance(pragueTimeZone);
        int i = getUpdateTimeIndex(calendar.get(Calendar.HOUR_OF_DAY));
        if (i < 0) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);
            i = updateTimes.length - 1;
        }
        calendar.set(Calendar.HOUR_OF_DAY, updateTimes[i]);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private int getUpdateTimeIndex(Date date) {
        Calendar calendar = Calendar.getInstance(pragueTimeZone);
        calendar.setTime(date);
        return getUpdateTimeIndex(calendar.get(Calendar.HOUR_OF_DAY));
    }

    private int getUpdateTimeIndex(int hour) {
        for (int i = updateTimes.length - 1; i >= 0; --i) {
            if (hour >= updateTimes[i]) {
                return i;
            }
        }
        // -1 means that it was updated before the first update that day
        return -1;
    }

    private int hourDiff(Date date) {
        long diff = Calendar.getInstance(pragueTimeZone).getTime().getTime() - date.getTime();
        return (int)(diff / hourMillis);
    }
}

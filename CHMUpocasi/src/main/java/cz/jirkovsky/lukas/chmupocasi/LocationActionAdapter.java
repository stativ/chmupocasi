/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * Adapter used to populate spinner that allows selecting the region.
 */
public class LocationActionAdapter extends BaseAdapter {
    private final Context context;
    private final RegionItem[] listOfRegions = new RegionItem[Region.values().length];

    private class RegionItem {
        private final String name;

        private RegionItem(int id) {
            name = context.getResources().getString(id);
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public LocationActionAdapter(Context context) {
        this.context = context;
        for (int i = 0; i < listOfRegions.length; ++i) {
            listOfRegions[i] = new RegionItem(Region.values()[i].getName());
        }
    }

    @Override
    public int getCount() {
        return listOfRegions.length;
    }

    @Override
    public RegionItem getItem(int i) {
        return listOfRegions[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.location_element, viewGroup, false);
        }
        ((TextView) view).setText(getItem(i).toString());
        return view;
    }
}

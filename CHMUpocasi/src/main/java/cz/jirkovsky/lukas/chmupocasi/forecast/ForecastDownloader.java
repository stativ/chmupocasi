/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import cz.jirkovsky.lukas.chmupocasi.MessageData;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.DayForecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Forecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.parsing.ForecastParser;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * Encapsulates downloading of forecast using an AsyncTask.
 */
@SuppressWarnings("WeakerAccess")
public class ForecastDownloader extends Fragment {
    // printf-like formatted URL with  a sing string (ie. %s) argument taking the region
    private final static String FORECAST_URL = "http://portal.chmi.cz/files/portal/docs/meteo/om/inform/p_%s.html";

    private Thread worker;
    private Handler handler;
    private DownloadForecastTask currentTask;
    private DownloadForecastTask waitingTask;
    private final Object taskLock = new Object();

    // the last-modified header field stored as a timestamp in ms
    // 0 if not known
    private long lastModified[];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        lastModified = new long[Region.values().length];
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public synchronized void download(Region region, boolean checkChanged) {
        // prepare the task (and possibly execute it)
        synchronized (taskLock) {
            // if there is no running task, execute it immediately
            if (worker == null || !worker.isAlive()) {
                currentTask = new DownloadForecastTask(region, checkChanged);
                worker = new Thread(currentTask);
                worker.start();
            }
            // if there is a running task, add a new task only if it's different from the running task
            else if (currentTask.getRegion() != region) {
                waitingTask = new DownloadForecastTask(region, checkChanged);
            }
        }
    }

    class DownloadForecastTask implements Runnable {
        private final Region region;
        private final boolean checkChanged;
        private boolean error;
        private boolean changed;

        private DownloadForecastTask(Region region, boolean checkChanged) {
            this.region = region;
            this.checkChanged = checkChanged;
            // start with no error
            error = false;
            changed = false;
        }

        public Region getRegion() {
            return region;
        }

        @Override
        public void run() {
            final ArrayList<DayForecast> forecast = downloadUrl(String.format(FORECAST_URL, region.getLocation()));
            onPostExecute(forecast);
        }

        private void onPostExecute(ArrayList<DayForecast> s) {
            // send error if the error flag is set
            if(error) {
                handler.sendMessage(Message.obtain(handler, 3, new MessageData(region, MessageData.TYPE.ERROR)));
            }
            // send success unchanged if the changed flag is not set
            else if (checkChanged && !changed) {
                handler.sendMessage(Message.obtain(handler, 2, new MessageData(region, MessageData.TYPE.SUCCESS_UNCHANGED)));
            }
            // send error if the forecast is missing
            else if(s == null || s.isEmpty()) {
                handler.sendMessage(Message.obtain(handler, 3, new MessageData(region, MessageData.TYPE.ERROR)));
            }
            // the update the cache
            else {
                handler.sendMessage(Message.obtain(handler, 1, new MessageData(region, new Forecast(s, lastModified[region.ordinal()]))));
            }

            // execute the waiting task, if there is some
            synchronized (taskLock) {
                if (waitingTask != null) {
                    currentTask = waitingTask;
                    worker = new Thread(currentTask);
                    worker.start();
                    waitingTask = null;
                }
            }
        }

        private ArrayList<DayForecast> downloadUrl(String url) {
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) (new URL(url)).openConnection();
                conn.setConnectTimeout(15000); // 15s
                conn.setReadTimeout(20000); // 20s
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // use the HTTP conditional request to download the forecast only if the page has changed
                if(checkChanged && lastModified[region.ordinal()] != 0) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
                    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String strLastModified = dateFormat.format(new Date(lastModified[region.ordinal()]));
                    conn.addRequestProperty("If-Modified-Since", strLastModified);
                }
                // Starts the query
                conn.connect();

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    changed = true;
                    lastModified[region.ordinal()] = conn.getLastModified();
                    // send the progress
                    handler.sendMessage(Message.obtain(handler, 0, new MessageData(region, MessageData.TYPE.PROGRESS)));
                    is = conn.getInputStream();

                    return ForecastParser.parse(is);
                }
                else if (conn.getResponseCode() == HttpURLConnection.HTTP_NOT_MODIFIED) {
                    changed = false;
                    return null;
                }
                else {
                    error = true;
                    return null;
                }
            } catch (Exception e) {
                error = true;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        // ignore exception here, as it's not problem
                    }
                }
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
    }
}

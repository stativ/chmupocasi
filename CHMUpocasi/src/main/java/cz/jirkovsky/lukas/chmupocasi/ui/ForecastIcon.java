/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import cz.jirkovsky.lukas.chmupocasi.R;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.WeatherFeatures;

/**
 * Creates a drawable icon representing the weather.
 */
public class ForecastIcon extends Drawable {
    private final Bitmap bitmap;
    private final boolean isDay;
    private final float px;
    private final float pxRound;

    public ForecastIcon(WeatherFeatures weatherFeatures, boolean isDay, Context context) {
        this.isDay = isDay;

        px = dpToPx(context, 48);
        pxRound = dpToPx(context, 8);

        bitmap = Bitmap.createBitmap((int) Math.ceil(px), (int) Math.ceil(px), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);

        drawBitmap(context, c, weatherFeatures);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, null);
    }

    private void drawBitmap(Context context, Canvas canvas, WeatherFeatures weatherFeatures) {
        // draw background
        Paint backgroundPaint = new Paint();
        backgroundPaint.setAntiAlias(true);
        if (isDay) {
            backgroundPaint.setColor(context.getResources().getColor(R.color.list_light_gray));
        }
        else {
            backgroundPaint.setColor(context.getResources().getColor(R.color.holo_dark_gray));
        }
        canvas.drawRoundRect(new RectF(0, 0, px, px), pxRound, pxRound, backgroundPaint);

        // cloud cover
        if (weatherFeatures.getCloudCover() > 0.8125) { // skoro zatazeno, zatazeno
            Bitmap clouds = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_clouds_overcast);
            canvas.drawBitmap(clouds, 0, 0, null);
        }
        else if (weatherFeatures.getCloudCover() > 0.5625) { // oblacno
            Bitmap clouds = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_clouds_basic);
            canvas.drawBitmap(clouds, 0, 0, null);
        }
        else {
            Bitmap clear;
            if (isDay) {
                clear = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_sun);
            }
            else {
                clear = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_moon);
            }

            if (weatherFeatures.getCloudCover() > 0.3125) { // polojasno
                Bitmap clouds = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_clouds_basic);
                canvas.drawBitmap(clear, 0, 0, null);
                canvas.drawBitmap(clouds, 0, 0, null);
            }
            else if (weatherFeatures.getCloudCover() > 0.125) { // skoro jasno
                Bitmap clouds = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_clouds_light);
                canvas.drawBitmap(clear, 0, 0, null);
                canvas.drawBitmap(clouds, 0, 0, null);
            } else { // jasno
                canvas.drawBitmap(clear, 0, 0, null);
            }
        }

        // draw rain
        if (weatherFeatures.getRain() > 0) {
            Bitmap rain = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_rain);
            if (weatherFeatures.getRain() > 0.75) {
                canvas.drawBitmap(rain, dpToPx(context, 3.3f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 10), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 16.7f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 23.3f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 30), 0, null);
            }
            else if (weatherFeatures.getRain() > 0.5) {
                canvas.drawBitmap(rain, dpToPx(context, 8), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 14.7f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 21.3f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 28), 0, null);
            }
            else if (weatherFeatures.getRain() > 0.25) {
                canvas.drawBitmap(rain, dpToPx(context, 7.3f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 16), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 25.3f), 0, null);
            }
            else if (weatherFeatures.getRain() > 0) {
                canvas.drawBitmap(rain, dpToPx(context, 13.3f), 0, null);
                canvas.drawBitmap(rain, dpToPx(context, 25.3f), 0, null);
            }
        }

        // draw showers
        if (weatherFeatures.getShowers() > 0) {
            Bitmap showers = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_showers);
            if (weatherFeatures.getShowers() > 0.66) {
                canvas.drawBitmap(showers, dpToPx(context, 6.7f), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 13.3f), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 20), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 26.7f), 0, null);
            }
            else if (weatherFeatures.getShowers() > 0.33) {
                canvas.drawBitmap(showers, dpToPx(context, 12), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 19.3f), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 26), 0, null);
            }
            else if (weatherFeatures.getShowers() > 0) {
                canvas.drawBitmap(showers, dpToPx(context, 12), 0, null);
                canvas.drawBitmap(showers, dpToPx(context, 19.3f), 0, null);
            }
        }

        // draw snow
        if (weatherFeatures.getSnow() > 0) {
            Bitmap snow = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_snow);
            // 36, 53
            canvas.drawBitmap(snow, dpToPx(context, 24), dpToPx(context, 35.3f), null);
            if (weatherFeatures.getSnow() > 0.25) {
                // 23, 43
                canvas.drawBitmap(snow, dpToPx(context, 15.3f), dpToPx(context, 28.7f), null);
                if (weatherFeatures.getSnow() > 0.5) {
                    // 46, 40
                    canvas.drawBitmap(snow, dpToPx(context, 30.7f), dpToPx(context, 26.7f), null);
                    if (weatherFeatures.getSnow() > 0.75) {
                        // 10, 52
                        canvas.drawBitmap(snow, dpToPx(context, 6.7f), dpToPx(context, 34.7f), null);
                    }
                }
            }

        }

        // draw storms
        if (weatherFeatures.getStorm() > 0) {
            Bitmap lightning = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_lightning);
            if (weatherFeatures.getStorm() > 0.5) {
                canvas.drawBitmap(lightning, dpToPx(context, 18), 0, null);
                canvas.drawBitmap(lightning, dpToPx(context, 26.7f), 4, null);
            }
            else {
                canvas.drawBitmap(lightning, dpToPx(context, 21.3f), 4, null);
            }
        }
    }

    @Override
    public int getIntrinsicWidth() {
        return (int) (px + 0.5);
    }

    @Override
    public int getIntrinsicHeight() {
        return (int) (px + 0.5);
    }

    @Override
    public void setAlpha(int alpha) {
        // ignored
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // ignored
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

    // for HDPI returns 1.5 * value
    private float dpToPx(Context context, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
    }
}

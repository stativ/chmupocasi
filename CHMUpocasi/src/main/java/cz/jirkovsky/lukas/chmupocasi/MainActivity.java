/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import cz.jirkovsky.lukas.chmupocasi.forecast.ForecastDownloader;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;
import cz.jirkovsky.lukas.chmupocasi.ui.AboutDialog;
import cz.jirkovsky.lukas.chmupocasi.ui.ForecastViewFragment;

public class MainActivity extends Activity {
    private ForecastViewFragment viewFragment;
    private ForecastDownloader forecastDownloader;
    private Region currentLocation;
    private RetainFragment retainFragment;

    private final Object waitingMessagesLock = new Object();
    private Handler handler;

    private boolean configurationChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getFragmentManager();
        // create cache fragment, which is retained during configuration changes
        retainFragment = (RetainFragment) fm.findFragmentByTag("cache");
        if (retainFragment == null) {
            retainFragment = new RetainFragment();
            fm.beginTransaction().add(retainFragment, "cache").commit();
        }
        forecastDownloader = (ForecastDownloader) fm.findFragmentByTag("forecastDownloader");
        if (forecastDownloader == null) {
            forecastDownloader = new ForecastDownloader();
            fm.beginTransaction().add(forecastDownloader, "forecastDownloader").commit();
        }
        viewFragment = (ForecastViewFragment) fm.findFragmentById(R.id.fragment);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                MessageData data = (MessageData) msg.obj;
                if (data != null) {
                    synchronized (waitingMessagesLock) {
                        retainFragment.getWaitingMessages().put(data.getRegion(), data);
                    }
                    processMessages();
                }
            }
        };

        forecastDownloader.setHandler(handler);

        // obtain the location from preferences
        currentLocation = Preferences.getLocation(this);

        // for tablets in landscape mode, show the location selection in the left column instead of the action bar
        if (isLandscapeTablet()) {
            final ListView regionList = (ListView) findViewById(R.id.regionListView);
            regionList.setAdapter(new LocationActionAdapter(this));
            regionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (currentLocation != Region.values()[position]) {
                        changeLocation(Region.values()[position]);
                    }
                }
            });
        }

        // this is used to determine whether the Activity was recreated due to a configuration change
        configurationChanged = savedInstanceState != null && savedInstanceState.getBoolean("confChange", false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // if onStart is called because of a configuration change, only show the forecast content from cache
        if (configurationChanged) {
            viewFragment.setForecast(retainFragment.getCache().getEntry(currentLocation));
        }
        // if onStart is called as a part of the standard Activity lifecycle, update the forecast
        else {
            cachedUpdateForecast();
            processMessages();
        }

        // select the current region in the region list, so it is properly highlighted for tablets in landscape mode
        if (isLandscapeTablet()) {
            final ListView regionList = (ListView) findViewById(R.id.regionListView);
            regionList.setItemChecked(currentLocation.ordinal(), true);
        }

        // make sure the gradients look nice
        getWindow().setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onPause() {
        super.onPause();
        configurationChanged = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if (! isLandscapeTablet()) {
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
                actionBar.setListNavigationCallbacks(new LocationActionAdapter(this), new LocationActionListener());
                // change the location to the current location
                actionBar.setSelectedNavigationItem(currentLocation.ordinal());
            }
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_weatherIcons).setChecked(Preferences.isWeatherIconsEnabled(this));
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("confChange", true);
    }

    private void processMessages() {
        MessageData data;
        synchronized (waitingMessagesLock) {
            data = retainFragment.getWaitingMessages().get(currentLocation);
            // progress is never removed to make sure the progress status is preserved between configuration changes
            // furthermore, after the processing finishes, it is always replaced by either success or error
            if (data != null && data.getType() != MessageData.TYPE.PROGRESS)
                retainFragment.getWaitingMessages().remove(currentLocation);
        }
        if (data != null) {
            switch (data.getType()) {
                case PROGRESS:
                    viewFragment.showProgress();
                    break;
                case SUCCESS:
                    retainFragment.getCache().addEntry(data.getRegion(), data.getForecast());
                    viewFragment.setForecast(data.getForecast());
                    viewFragment.showSuccess();
                    break;
                case SUCCESS_UNCHANGED:
                    // show the success message only if the progress was previously shown
                    // used for the "RECENT" download that shows status only if it downloads something
                    if (viewFragment.getStatusState() != ForecastViewFragment.StatusState.HIDDEN) {
                        viewFragment.showSuccess();
                    }
                    break;
                case ERROR:
                    // see SUCCESS_UNCHANGED
                    if (viewFragment.getStatusState() != ForecastViewFragment.StatusState.HIDDEN) {
                        viewFragment.showError();
                    }
                    break;
            }
        }
    }

    private void cachedUpdateForecast() {
        // get the freshness
        switch (retainFragment.getCache().getFreshness(currentLocation)) {
            case FRESH:
                // don't update anything, also don't do any cleanup
                viewFragment.setForecast(retainFragment.getCache().getEntry(currentLocation));
                break;
            case RECENT:
                // first set the forecast from cache
                viewFragment.setForecast(retainFragment.getCache().getEntry(currentLocation));
                // try to update on background, if this fails, the previously set forecast will be used
                // the progress will be shown by downloader only if something is downloaded
                updateForecast(true);
                break;
            case OLD:
                // first set the forecast from cache
                viewFragment.setForecast(retainFragment.getCache().getEntry(currentLocation));
                // update forecast
                // we always show the progress in this case
                viewFragment.showProgress();
                updateForecast(false);
                break;
        }
    }

    /**
     * Download forecast
     *
     * @param checkChanged If true, the forecast is downloaded only if the server response header
     *                     suggests the forecast has changed without actually downloading the forecast
     */
    private void updateForecast(boolean checkChanged) {
        // information about connectivity
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            forecastDownloader.download(currentLocation, checkChanged);
        }
        else {
            handler.sendMessage(Message.obtain(handler, 3, new MessageData(currentLocation, MessageData.TYPE.ERROR)));
        }
    }

    private void changeLocation(Region region) {
        currentLocation = region;
        // save the selected location into preferences
        Preferences.setLocation(this, currentLocation);
        // reset the status
        viewFragment.hideAllStatuses();
        // update the forecast after the location has changed
        cachedUpdateForecast();
        // make sure that the messages are processed if there are any
        processMessages();
    }

    private boolean isLandscapeTablet() {
        final int screenLayout = getResources().getConfiguration().screenLayout;
        final boolean large = ((screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        final boolean xlarge = ((screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        return (large || xlarge) && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    private class LocationActionListener implements ActionBar.OnNavigationListener {
        @Override
        public boolean onNavigationItemSelected(int i, long l) {
            if (currentLocation != Region.values()[i]) {
                changeLocation(Region.values()[i]);
            }
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_update:
                // force show progress
                viewFragment.showProgress();
                updateForecast(false);
                return true;
            case R.id.action_about:
                DialogFragment aboutDialog = new AboutDialog();
                aboutDialog.show(getFragmentManager(), "aboutDialog");
                return true;
            case R.id.action_weatherIcons:
                boolean enable = ! item.isChecked();
                viewFragment.enableWeatherIcons(enable);
                item.setChecked(enable);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

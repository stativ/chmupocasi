/*
 *   Copyright 2014-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.ui;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.Animatable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import cz.jirkovsky.lukas.chmupocasi.Preferences;
import cz.jirkovsky.lukas.chmupocasi.R;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Forecast;

/**
 * A fragment that shows forecast including various state messages.
 */
public class ForecastViewFragment extends Fragment {
    private ForecastViewAdapter forecastViewAdapter;

    private ListView forecastView;
    private LinearLayout progress;
    private LinearLayout status;
    private TextView statusText;
    private ImageView statusIcon;

    public enum StatusState {
        HIDDEN,
        PROGRESS,
        SUCCESS,
        ERROR
    }
    private StatusState statusState;

    // used when resetting padding after the status is hidden
    private int defaultPaddingBottom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusState = StatusState.HIDDEN;

        // restore the state here, too to make sure we have it as soon as possible
        if (savedInstanceState != null) {
            statusState = StatusState.values()[savedInstanceState.getInt("status", 0)];
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        forecastViewAdapter = new ForecastViewAdapter(activity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("status", statusState.ordinal());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forecast_view, container, false);

        forecastView = (ListView) view.findViewById(R.id.forecastView);
        forecastView.setAdapter(forecastViewAdapter);
        forecastView.setItemsCanFocus(true);

        progress = (LinearLayout) view.findViewById(R.id.forecastViewProgress);
        status = (LinearLayout) view.findViewById(R.id.forecastViewStatus);
        statusText = (TextView) view.findViewById(R.id.forecastViewStatusText);
        statusIcon = (ImageView) view.findViewById(R.id.forecastViewStatusIcon);

        // the padding is always zero (check with the forecast_view layout)
        // the value is hardcoded to make sure there are no race-conditions
        // that would cause defaultPaddingBottom to contain the padding for status
        defaultPaddingBottom = 0;
        // make sure the bottom padding is really zero, even if the view was recreated after low-mem condition
        forecastView.setPadding(forecastView.getPaddingLeft(), forecastView.getPaddingTop(), forecastView.getPaddingRight(), defaultPaddingBottom);

        // restore the state
        if (savedInstanceState != null) {
            statusState = StatusState.values()[savedInstanceState.getInt("status", 0)];
        }
        switch (statusState) {
            case PROGRESS:
                showProgress();
                break;
            case SUCCESS:
                showSuccess();
                break;
            case ERROR:
                showError();
                break;
        }

        return view;
    }

    public StatusState getStatusState() {
        return statusState;
    }

    public void setForecast(Forecast forecast) {
        forecastViewAdapter.setForecast(forecast);
    }

    public void enableWeatherIcons(boolean enabled) {
        Preferences.setWeatherIconsEnabled(getActivity(), enabled);
        // force redraw the forecast
        forecastViewAdapter.notifyDataSetChanged();
    }

    public void hideAllStatuses() {
        statusState = StatusState.HIDDEN;

        progress.setVisibility(View.GONE);
        status.setVisibility(View.GONE);
        // reset the padding
        forecastView.setPadding(forecastView.getPaddingLeft(), forecastView.getPaddingTop(), forecastView.getPaddingRight(), defaultPaddingBottom);
    }

    public void showProgress() {
        statusState = StatusState.PROGRESS;

        if (forecastViewAdapter.getCount() == 0) {
            progress.setVisibility(View.VISIBLE);
        }
        statusText.setText(R.string.status_progress);
        statusIcon.setImageResource(R.drawable.progress_medium_white);
        ((Animatable) statusIcon.getDrawable()).start();
        showStatus();
    }

    public void showSuccess() {
        statusState = StatusState.SUCCESS;

        progress.setVisibility(View.GONE);
        statusText.setText(R.string.status_success);
        statusIcon.setImageResource(R.drawable.ic_status_success);
        showStatusTemporary();

        // start timer to automatically hide the status after some time
        CountDownTimer timer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                hideAllStatuses();
            }
        };
        timer.start();
    }

    public void showError() {
        statusState = StatusState.ERROR;

        statusText.setText(R.string.status_error);
        statusIcon.setImageResource(R.drawable.ic_status_error);
        showStatusTemporary();
    }

    /** Shows a status bar on the bottom of the fragment.
     * The status bar can not be hidden by user.
     */
    private void showStatus() {
        status.setVisibility(View.VISIBLE);
        // delay this until the view has been created to make sure status.getHeight() returns height rather than 0
        final ViewTreeObserver observer = status.getViewTreeObserver();
        if (observer != null) {
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    forecastView.setPadding(forecastView.getPaddingLeft(), forecastView.getPaddingTop(),
                            forecastView.getPaddingRight(), defaultPaddingBottom + status.getHeight());
                    progress.setPadding(progress.getPaddingLeft(), progress.getTop(),
                            progress.getPaddingRight(), status.getHeight());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        status.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        status.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
            });
        }

    }

    /** Show a hideable status bar on the bottom
     * The user can hide the status bar by clicking it.
     */
    private void showStatusTemporary() {
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideAllStatuses();
                status.setOnClickListener(null);
            }
        });
        showStatus();
    }
}

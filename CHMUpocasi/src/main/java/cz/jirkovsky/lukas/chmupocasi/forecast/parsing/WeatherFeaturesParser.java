/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.parsing;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.WeatherFeatures;

/**
 * Class providing parsing functionality to obtain weather features from forecast text.
 *
 * The values for quantifiers are based on information from:
 *  http://pocasi.chmi.cz/HK/metter.htm
 */
public class WeatherFeaturesParser {
    private static class ParserState {
        double features[];
        int featuresCount[];
        double timeSpanQuantifier;
        double lastQuantifier;
        double quantifier;
        int skip;

        ParserState() {
            features = new double[WeatherFeatures.WeatherFeature.values().length];
            featuresCount = new int[WeatherFeatures.WeatherFeature.values().length];
            timeSpanQuantifier = 1.0;
            lastQuantifier = 1.0;
            quantifier = 1.0;
            skip = 0;
        }

        // Functions used by the parsers

        /**
         * Add a new feature.
         *
         * The function is used by the parser.
         *
         * @param feature feature to add
         * @param value the weight of the feature
         */
        void addFeature(WeatherFeatures.WeatherFeature feature, double value) {
            addFeatureNoInc(feature, value);
            featuresCount[feature.ordinal()]++;
        }
        /**
         * Add a new feature without incrementing its count.
         *
         * @param feature feature to add
         * @param value the weight of the feature
         */
        void addFeatureNoInc(WeatherFeatures.WeatherFeature feature, double value) {
            features[feature.ordinal()] = features[feature.ordinal()] + value;
        }

        void setQuantifier(double value) {
            lastQuantifier = value;
            quantifier = value;
        }

        void setTimeSpanQuantifier(double value) {
            timeSpanQuantifier = value;
        }

        void resetQuantifier() {
            lastQuantifier = quantifier;
            quantifier = 1.0;
        }

        /**
         * Get current quantifier.
         * @return the quantifier
         */
        double getQuantifier() {
            return quantifier;
        }

        /**
         * Get quantifier with time span quantifier applied to it.
         * @return the quantifier
         */
        double getFinalQuantifier() {
            return timeSpanQuantifier * quantifier;
        }
    }

    private static final ParserTrie<ParserState> parserTrie;

    /**
     * Parse forecast to obtain weather features
     *
     * @param forecast text of forecast to process
     * @return WeatherFeatures object containing weather features
     */
    public static WeatherFeatures parse(String forecast) {
        int pos = 0;
        ParserState state = new ParserState();

        while (pos < forecast.length()) {
            ParserTrie.Value<ParserState> value = parserTrie.get(forecast, pos);
            if (value != null) {
                // skip the already parsed part of the string. The -1 is needed in case the keyword ends with ' '
                pos = consume(forecast, value.end - 1);
                if (--state.skip >= 0) {
                    state.timeSpanQuantifier = 1.0;
                    state.lastQuantifier = 1.0;
                    state.quantifier = 1.0;
                } else {
                    value.parser.run(state);
                }
            }
            else {
                // skip to the next word
                int tmp = forecast.indexOf(' ', pos);
                if (tmp >= 0)
                    pos = tmp + 1;
                else
                    break;
            }

            // check if we have crossed the sentence boundary
            // it is pos -2 because "pos - 2" is the last character, "pos - 1" is space and "pos" is the next character
            if (pos > 2 && forecast.charAt(pos - 2) == '.') {
                state.timeSpanQuantifier = 1.0;
                state.lastQuantifier = 1.0;
                state.quantifier = 1.0;
                state.skip = 0;
            }
        }

        // average everything as needed
        for (int i = 0; i < WeatherFeatures.WeatherFeature.values().length; ++i) {
            if (state.featuresCount[i] > 0)
                state.features[i] = state.features[i] / state.featuresCount[i];
            else
                state.features[i] = 0.0;
        }

        return new WeatherFeatures(state.features);
    }

    /**
     * Static initialization of a trie used for parsing
     */
    static {
        parserTrie = new ParserTrie<>();

        /***************/
        /* QUANTIFIERS */
        /***************/

        parserTrie.put("celé? území", new ParserTrie.FeatureParser<ParserState>() { // 70% – 100%
            @Override
            public void run(ParserState state) {
                state.setQuantifier((state.getQuantifier() < 1.0) ? 0.5*(state.getQuantifier() + 0.85) : 0.85);
            }
        });
        parserTrie.put("většin? území", new ParserTrie.FeatureParser<ParserState>() { // 50% – 69%
            @Override
            public void run(ParserState state) {
                state.setQuantifier((state.getQuantifier() < 1.0) ? 0.5*(state.getQuantifier() + 0.6) : 0.6);
            }
        });
        parserTrie.put("místy", new ParserTrie.FeatureParser<ParserState>() { // 30% – 49%
            @Override
            public void run(ParserState state) {
                state.setQuantifier((state.getQuantifier() < 1.0) ? 0.5*(state.getQuantifier() + 0.4) : 0.4);
            }
        });
        parserTrie.put("ojediněl", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setQuantifier((state.getQuantifier() < 1.0) ? 0.5*(state.getQuantifier() + 0.175) : 0.175);
            }
        });
        // this is not a quantifier for the affected area, but it is often used in forecast
        // quantifier 0.5 seems like a sane value
        parserTrie.put("občas", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setQuantifier((state.getQuantifier() < 1.0) ? 0.5*(state.getQuantifier() + 0.5) : 0.5);
            }
        });

        /***************/
        /* TIME SPANS  */
        /***************/
        // The time span is used to reduce the quantifier in case the phenomenon is only for a limited time
        // eg. if the rain is only in the evening, it shouldn't be handled the same way as when it's raining all day
        parserTrie.put("ráno", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setTimeSpanQuantifier(0.75);
            }
        });
        parserTrie.put("odpoledne", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setTimeSpanQuantifier(0.85);
            }
        });
        parserTrie.put("večer", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setTimeSpanQuantifier(0.5);
            }
        });

        /****************/
        /* CONJUNCTIONS */
        /****************/
        // conjunctions keeps the quantifier
        // because all features resets the quantifier, they set it back the previous one
        ParserTrie.FeatureParser<ParserState> conjunctionParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.setQuantifier(state.lastQuantifier);
            }
        };
        parserTrie.put("nebo ", conjunctionParser);
        parserTrie.put("až ", conjunctionParser);

        /*********************/
        /* SUN & CLOUD COVER */
        /*********************/
        parserTrie.put("jasno", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, 0);
                state.resetQuantifier();
            }
        });
        parserTrie.put("skoro jasno", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, state.getFinalQuantifier() * 0.1875);
                state.resetQuantifier();
            }
        });
        parserTrie.put("polojasno", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, state.getFinalQuantifier() * 0.4375);
                state.resetQuantifier();
            }
        });
        parserTrie.put("oblačno", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, state.getFinalQuantifier() * 0.6875);
                state.resetQuantifier();
            }
        });
        parserTrie.put("skoro zataženo", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, state.getFinalQuantifier() * 0.875);
                state.resetQuantifier();
            }
        });
        ParserTrie.FeatureParser<ParserState> overcastParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.CLOUD_COVER, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        };
        parserTrie.put("zataženo", overcastParser);
        parserTrie.put("zataženo nízkou oblačností", overcastParser);

        ParserTrie.FeatureParser<ParserState> reduceCloudCoverParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                // reduce the existing cloud cover a bit WITHOUT! averaging
                if (state.featuresCount[WeatherFeatures.WeatherFeature.CLOUD_COVER.ordinal()] > 0) {
                    double val = - state.getFinalQuantifier() * 0.35 *
                            state.features[WeatherFeatures.WeatherFeature.CLOUD_COVER.ordinal()] / state.featuresCount[WeatherFeatures.WeatherFeature.CLOUD_COVER.ordinal()];
                    state.addFeatureNoInc(WeatherFeatures.WeatherFeature.CLOUD_COVER, val);
                }
                state.resetQuantifier();
            }
        };
        parserTrie.put("ubývání oblačn", reduceCloudCoverParser);
        parserTrie.put("protrhávání oblačn", reduceCloudCoverParser);

        ParserTrie.FeatureParser<ParserState> reduceRainParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                // reduce the existing cloud cover a bit WITHOUT! averaging
                if (state.featuresCount[WeatherFeatures.WeatherFeature.RAIN.ordinal()] > 0) {
                    double val = - state.getFinalQuantifier() * 0.3 *
                            state.features[WeatherFeatures.WeatherFeature.RAIN.ordinal()] / state.featuresCount[WeatherFeatures.WeatherFeature.RAIN.ordinal()];
                    state.addFeatureNoInc(WeatherFeatures.WeatherFeature.RAIN, val);
                }
                if (state.featuresCount[WeatherFeatures.WeatherFeature.SHOWERS.ordinal()] > 0) {
                    double val = - state.getFinalQuantifier() * 0.3 *
                            state.features[WeatherFeatures.WeatherFeature.SHOWERS.ordinal()] / state.featuresCount[WeatherFeatures.WeatherFeature.SHOWERS.ordinal()];
                    state.addFeatureNoInc(WeatherFeatures.WeatherFeature.SHOWERS, val);
                }
                state.resetQuantifier();
            }
        };
        parserTrie.put("slábnutí sráž", reduceRainParser);
        parserTrie.put("ubývání sráž", reduceRainParser);
        parserTrie.put("ustávání sráž", reduceRainParser);

        /****************************/
        /* PARSER THAT DOES NOTHING */
        /****************************/
        // it just skips the word and decrements state.skip

        ParserTrie.FeatureParser<ParserState> ignoreParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                // ignore
                state.resetQuantifier();
            }
        };

        /**********************/
        /*    RAIN & SNOW     */
        /**********************/
        ParserTrie.FeatureParser<ParserState> rainParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.RAIN, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        };
        parserTrie.put("déšť", rainParser);
        parserTrie.put("dešt", rainParser);
        parserTrie.put("dešť", rainParser);

        parserTrie.put("přeháňk", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.SHOWERS, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        });

        // TODO
        parserTrie.put("mrholení", ignoreParser);

        parserTrie.put("bouřk", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.STORM, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        });
        // usually something along the way "v bouřkách vítr přechodně zesílí", so ignore
        parserTrie.put("bouřkách", ignoreParser);

        ParserTrie.FeatureParser<ParserState> snowParser = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.SNOW, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        };
        parserTrie.put("sníh", snowParser);
        parserTrie.put("sněh", snowParser);
        parserTrie.put("sněž", snowParser);

        ParserTrie.FeatureParser<ParserState> snowParser2 = new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.SNOW, state.getFinalQuantifier() * 0.75);
                state.resetQuantifier();
            }
        };

        parserTrie.put("sněhové přeháňky", snowParser2);
        parserTrie.put("sněhovým? přeháňk", snowParser2);

        parserTrie.put("smíšené", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.SNOW, state.getFinalQuantifier() * 0.5);
                state.addFeature(WeatherFeatures.WeatherFeature.RAIN, state.getFinalQuantifier() * 0.5);
                state.resetQuantifier();
            }
        });

        parserTrie.put("sněhové jazyky", ignoreParser);
        parserTrie.put("sněhových jazyků", ignoreParser);

        parserTrie.put("mlh", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.addFeature(WeatherFeatures.WeatherFeature.FOG, state.getFinalQuantifier());
                state.resetQuantifier();
            }
        });

        /****************/
        /*    OTHER     */
        /****************/
        // skip the feature if it's used in a conditional statement
        parserTrie.put("při ", new ParserTrie.FeatureParser<ParserState>() {
            @Override
            public void run(ParserState state) {
                state.skip = 1;
                state.resetQuantifier();
            }
        });

        // special case for Prague, because it may contain
        //  "Předpokládané množství srážek/nový sníh"
        // which should not be matched as snow
        parserTrie.put("srážek/nový sníh", ignoreParser);
    }

    /**
     * Consume string until next space (including the space)
     * @param string string to process
     * @param pos position where the process starts
     * @return position of the next word in a string or
     *         string.length if there is no other word.
     */
    private static int consume(String string, int pos) {
        int tmp = string.indexOf(' ', pos);
        return (tmp >= 0) ? tmp + 1 : string.length();
    }
}

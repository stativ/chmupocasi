/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.data;

import java.util.Calendar;

import cz.jirkovsky.lukas.chmupocasi.R;

/**
 * Enumeration of possible values for day in the forecast.
 *
 * The enum includes some special values that are not calendar days, but which
 * are often used in the forecast.
 */
public enum Days {
    UNKNOWN(R.string.day_unknown, -1),

    NIGHT(R.string.day_night, -1),

    Mon(R.string.day_mon, Calendar.MONDAY),
    Tue(R.string.day_tue, Calendar.TUESDAY),
    Wed(R.string.day_wed, Calendar.WEDNESDAY),
    Thu(R.string.day_thu, Calendar.THURSDAY),
    Fri(R.string.day_fri, Calendar.FRIDAY),
    Sat(R.string.day_sat, Calendar.SATURDAY),
    Sun(R.string.day_sun, Calendar.SUNDAY);

    /**
     * Return the resource id of the day name.
     *
     * @return resource id of the day name
     */
    public int getName() {
        return name;
    }

    /**
     * Return the value of the day as used in java.util.Calendar.
     *
     * If the enum value is not a valid day (eg. night), the returned value is -1
     *
     * @return calendar day or -1
     */
    public int getCalendarDay() {
        return calendarDay;
    }

    /**
     * A constructor.
     *
     * @param name name resource id
     * @param calendarDay day as defined by java.util.Calendar
     */
    Days(int name, int calendarDay) {
        this.name = name;
        this.calendarDay = calendarDay;
    }

    private final int name;
    private final int calendarDay;
}

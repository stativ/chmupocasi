/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;
import android.util.LruCache;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jirkovsky.lukas.chmupocasi.BuildConfig;
import cz.jirkovsky.lukas.chmupocasi.Preferences;
import cz.jirkovsky.lukas.chmupocasi.R;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.DayForecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Days;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Forecast;

/**
 * Adapter used to show the forecast using a ListView.
 */
// http://www.piwai.info/android-adapter-good-practices/
class ForecastViewAdapter extends BaseAdapter {
    private Forecast forecast;
    private LruCache<DayForecast, Drawable> iconCache;
    private final Context context;
    private final LayoutInflater inflater;
    private final int fourdp;

    private static final TimeZone TIMEZONE_PRAGUE = TimeZone.getTimeZone("Europe/Prague");
    private final SimpleDateFormat FORMAT_DATE;
    private final SimpleDateFormat FORMAT_TIME;

    private static class TextMarginForIcon implements LeadingMarginSpan.LeadingMarginSpan2 {
        private final int margin;
        private final int marginLineCount;

        private TextMarginForIcon(int margin, float height, float textSize) {
            this.margin = margin;
            marginLineCount = Math.round(height / textSize);
        }

        @Override
        public int getLeadingMarginLineCount() {
            return marginLineCount;
        }

        @Override
        public int getLeadingMargin(boolean first) {
            if (first) {
                return margin;
            }
            else {
                return 0;
            }
        }

        @Override
        public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {

        }
    }

    public ForecastViewAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        forecast = null;
        iconCache = new LruCache<>(10);
        fourdp =  (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, context.getResources().getDisplayMetrics()) + 0.5);

        Locale locale = new Locale("cs", "CZ");
        FORMAT_DATE = new SimpleDateFormat("d.M.yyyy", locale);
        FORMAT_DATE.setTimeZone(TIMEZONE_PRAGUE);
        FORMAT_TIME = new SimpleDateFormat("H:mm", locale);
        FORMAT_TIME.setTimeZone(TIMEZONE_PRAGUE);
    }

    public void setForecast(Forecast forecast) {
        ThreadPreconditions.checkOnMainThread();
        this.forecast = forecast;
        if (forecast != null) {
            notifyDataSetChanged();
        } else {
            notifyDataSetInvalidated();
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (forecast == null || forecast.getForecast() == null) {
            return 0;
        }
        else if (position == forecast.getForecast().size()) {
            return 1;
        }
        else {
            return 0;
        }
    }

    @Override
    public int getCount() {
        if (forecast == null || forecast.getForecast() == null) {
            return 0;
        }
        else if(forecast.getTime() != 0) {
            return forecast.getForecast().size() + 1;
        }
        else {
            return forecast.getForecast().size();
        }
    }

    @Override
    public Object getItem(int i) {
        if (i < forecast.getForecast().size()) {
            return forecast.getForecast().get(i);
        }
        else {
            return forecast.getTime();
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (getItemViewType(i) == 0) {
            DayForecast dayForecast = ((DayForecast) getItem(i));
            return getForecastView(dayForecast, view, viewGroup);
        }
        else {
            long time = (long) getItem(i);
            return getForecastTimeView(time, view, viewGroup);
        }
    }

    private View getForecastTimeView(long time, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.list_forecasttime, viewGroup, false);
        }
        final TextView forecasttime = (TextView) view.findViewById(R.id.forecasttime);

        Date date = new Date(time);
        String strDate = FORMAT_DATE.format(date);
        String strTime = FORMAT_TIME.format(date);
        forecasttime.setText(context.getString(R.string.list_forecasttime, strDate, strTime));
        return view;
    }

    private View getForecastView(DayForecast dayForecast, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.list_dayforecast, viewGroup, false);
        }

        final TextView dayView = (TextView) view.findViewById(R.id.dayView);
        final ViewSwitcher switcher = (ViewSwitcher) view.findViewById(R.id.viewSwitcher);
        final TextView headerLeft = (TextView) view.findViewById(R.id.listHeadLeft);
        final TextView headerRight = (TextView) view.findViewById(R.id.listHeadRight);
        final View switchLeft = view.findViewById(R.id.switchLeft);
        final View switchRight = view.findViewById(R.id.switchRight);
        final LinearLayout content = (LinearLayout) view.findViewById(R.id.listContent);
        final TextView situationContentView  = (TextView) view.findViewById(R.id.situationContentView);
        final ImageView forecastIconView = (ImageView) view.findViewById(R.id.forecastIconView);
        final TextView forecastTextView = (TextView) view.findViewById(R.id.forecastTextView);

        // try to enable dithering for the day name background, so the gradient doesn't have ugly banding
        if (dayView.getBackground() != null) {
            dayView.getBackground().setDither(true);
        }

        /*
         * Set the content of the views
         */
        dayView.setText(context.getString(dayForecast.getMatchingDay().getName()));

        if (dayForecast.getSituation() != null && dayForecast.getSituation().length() != 0) {
            situationContentView.setText(dayForecast.getSituation());
        }
        else {
            situationContentView.setText(R.string.situation_not_available);
        }

        SpannableString ss = highlightTemperature(dayForecast.getForecast());
        if (Preferences.isWeatherIconsEnabled(context)) {
            Drawable forecastIcon = iconCache.get(dayForecast);
            if (forecastIcon == null) {
                forecastIcon = new ForecastIcon(dayForecast.getFeatures(), dayForecast.getMatchingDay() != Days.NIGHT , context);
                iconCache.put(dayForecast, forecastIcon);
            }
            forecastIconView.setVisibility(View.VISIBLE);
            forecastIconView.setImageDrawable(forecastIcon);
            ss.setSpan(new TextMarginForIcon(forecastIcon.getIntrinsicWidth() + fourdp,forecastIcon.getIntrinsicHeight(), forecastTextView.getTextSize()),
                       0, ss.length(), 0);
        } else {
            forecastIconView.setVisibility(View.GONE);
        }
        forecastTextView.setText(ss);

        /*
         * Handle switching between the forecast and situation
         */
        switcher.setInAnimation(context, android.R.anim.fade_in);
        switcher.setOutAnimation(context, android.R.anim.fade_out);

        final ContentSwitcher contentSwitcher = new ContentSwitcher(view);
        final GestureDetector detector = new GestureDetector(context, contentSwitcher);

        content.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });

        View.OnClickListener switchLeftListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentSwitcher.switchToForecast();
            }
        };
        View.OnClickListener switchRightListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentSwitcher.switchToSituation();
            }
        };

        headerLeft.setOnClickListener(switchLeftListener);
        headerRight.setOnClickListener(switchRightListener);
        switchLeft.setOnClickListener(switchLeftListener);
        switchRight.setOnClickListener(switchRightListener);

        return view;
    }

    private SpannableString highlightTemperature(String forecast) {
        SpannableString ss = new SpannableString(forecast);
        Matcher matcher = Pattern.compile("([+-]?[0-9][0-9]? až )?[+-]?[0-9][0-9]? °?C").matcher(forecast);
        while (matcher.find()) {
            ss.setSpan(new StyleSpan(Typeface.BOLD), matcher.start(), matcher.end(), 0);
        }
        return ss;
    }

    private class ContentSwitcher extends GestureDetector.SimpleOnGestureListener {
        private TextView headerLeft;
        private TextView headerRight;
        private ViewSwitcher switcher;
        private View forecastView;
        private View situationView;

        private ContentSwitcher(View view) {
            switcher = (ViewSwitcher) view.findViewById(R.id.viewSwitcher);
            headerLeft = (TextView) view.findViewById(R.id.listHeadLeft);
            headerRight = (TextView) view.findViewById(R.id.listHeadRight);

            forecastView = view.findViewById(R.id.forecastView);
            situationView = view.findViewById(R.id.situationView);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            float absX = Math.abs(distanceX);
            float absY = Math.abs(distanceY);
            if (absX > absY && absX > 30.0) {
                switchContent(e1, e2);
            }
            return false;
        }

        private void switchContent(MotionEvent e1, MotionEvent e2) {
            if (e1.getRawX() > e2.getRawX()) {
                switchToSituation();
            } else {
                switchToForecast();
            }
        }

        public void switchToSituation() {
            if (switcher.getCurrentView() == forecastView) {
                headerLeft.setTypeface(Typeface.DEFAULT);
                headerRight.setTypeface(Typeface.DEFAULT_BOLD);
                switcher.showNext();
            }
        }

        public void switchToForecast() {
            if (switcher.getCurrentView() == situationView) {
                headerLeft.setTypeface(Typeface.DEFAULT_BOLD);
                headerRight.setTypeface(Typeface.DEFAULT);
                switcher.showPrevious();
            }
        }
    }

    private static class ThreadPreconditions {
        public static void checkOnMainThread() {
            if (BuildConfig.DEBUG) {
                if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                    throw new IllegalStateException("This method should be called from the Main Thread");
                }
            }
        }
    }
}

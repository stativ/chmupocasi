/*
 *   Copyright 2014-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

/**
 * The complete forecast
 */
public class Forecast implements Serializable {
    private final ArrayList<DayForecast> forecast;
    private final long time;

    // avoid requesting the time zone every time we need it
    private static final TimeZone pragueTimeZone = TimeZone.getTimeZone("Europe/Prague");
    // number of milliseconds in hour
    private static final long hourMillis = 60L * 60L * 1000L;

    /**
     * A constructor
     *
     * @param forecast list of forecast for each day
     * @param time time when the forecast was made as Unix time in ms or 0
     */
    public Forecast(ArrayList<DayForecast> forecast, long time) {
        this.forecast = forecast;
        this.time = time;
    }

    /**
     * Get the stored forecast
     */
    public ArrayList<DayForecast> getForecast() {
        return forecast;
    }

    /**
     * Get the time for the stored forecast as Unix time in ms
     */
    public long getTime() {
        return time;
    }

    /**
     * Remove the old entries from the forecast
     */
    public void cleanupForecast() {
        for (Iterator<DayForecast> it = forecast.iterator(); it.hasNext();) {
            DayForecast dayForecast = it.next();
            Date date = dayForecast.getDate();
            if (date != null) {
                long diff = Calendar.getInstance(pragueTimeZone).getTime().getTime() - date.getTime();
                // if the forecast is older than a day, remove it
                // the night is cleaned up earlier - after nine hours (meaning at 9 AM)
                if (diff > 24L * hourMillis ||
                        dayForecast.getMatchingDay() == Days.NIGHT && diff > 9L * hourMillis)
                {
                    it.remove();
                } else {
                    break;
                }
            } else {
                // don't remove anything if we don't know the date
                // in theory we could remove it based on a date of the following day,
                // but this shouldn't happen (or not very often) anyway
                break;
            }
        }
    }
}

/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi;

import android.content.Context;
import android.content.SharedPreferences;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * A class storing preference names.
 */
public class Preferences {
    private final static String PREFERENCE_FILE = "cz.jirkovsky.lukas.chmu";
    private final static String LOCATION = "location";
    private final static String ICONS = "icons";

    public static boolean isWeatherIconsEnabled(Context context) {
        return context.getSharedPreferences(Preferences.PREFERENCE_FILE, Context.MODE_PRIVATE).getBoolean(Preferences.ICONS, true);
    }

    public static void setWeatherIconsEnabled(Context context, boolean enabled) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Preferences.PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Preferences.ICONS, enabled);
        editor.apply();
    }

    public static Region getLocation(Context context) {
        int location = context.getSharedPreferences(Preferences.PREFERENCE_FILE, Context.MODE_PRIVATE).getInt(Preferences.LOCATION, 0);
        return Region.values()[location];
    }

    public static void setLocation(Context context, Region location) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Preferences.PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Preferences.LOCATION, location.ordinal());
        editor.apply();
    }
}

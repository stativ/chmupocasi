/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.data;

import cz.jirkovsky.lukas.chmupocasi.R;

/**
 * Represents a region.
 */
public enum Region {
    KARLOVARSKY(R.string.kraj_karlovarsky, "kv"),
    PLZENSKY(R.string.kraj_plzensky, "pl"),
    USTECKY(R.string.kraj_ustecky, "ul"),
    STREDOCESKY(R.string.kraj_stredocesky, "sc"),
    PRAHA(R.string.kraj_praha, "ph"),
    JIHOCESKY(R.string.kraj_jihocesky, "cb"),
    LIBERECKY(R.string.kraj_liberecky, "lb"),
    KRALOVEHRADECKY(R.string.kraj_kralovehradecky, "hk"),
    PARDUBICKY(R.string.kraj_pardubicky, "pu"),
    VYSOCINA(R.string.kraj_vysocina, "vy"),
    OLOMOUCKY(R.string.kraj_olomoucky, "ol"),
    JIHOMORAVSKY(R.string.kraj_jihomoravsky, "jm"),
    MORAVSKOSLEZSKY(R.string.kraj_moravskoslezsky, "ms"),
    ZLINSKY(R.string.kraj_zlinsky, "zl");

    /**
     * Return the resource id of the region name.
     *
     * @return resource id of the region name
     */
    public int getName() {
        return name;
    }

    /**
     * Get location string used to construct download URL.
     *
     * @return the location used in URL
     */
    public String getLocation() {
        return location;
    }

    /**
     * A constructor.
     *
     * @param name name resource id
     * @param location location string to construct download URL
     */
    Region(int name, String location) {
        this.name = name;
        this.location = location;
    }

    private final int name;
    private final String location;
}
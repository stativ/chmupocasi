/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.parsing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.DayForecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Days;

/**
 * Provides means to parse incoming HTML webpage into the list of DayForecast objects for each day.
 */
public class ForecastParser {
    // avoid requesting the time zone every time we need it
    private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("Europe/Prague");
    // avoid creation of locale every time we need it
    private static final Locale LOCALE = new Locale("cs", "CZ");
    // avoid creation of charset...
    private static final Charset CHARSET = Charset.forName("windows-1250");

    public static ArrayList<DayForecast> parse(InputStream is) throws IOException {
        ArrayList<DayForecast> forecast = new ArrayList<>(4);
        HashMap<Days, String> precipitation = new HashMap<>(6);
        BufferedReader reader =  new BufferedReader(new InputStreamReader(is, CHARSET));
        StringBuilder builderSituation = new StringBuilder();
        StringBuilder builderForecast = new StringBuilder();
        String line;
        ArrayList<Days> tmpDayEnum;
        ArrayList<Days> previousDayEnum = null;
        boolean parsing = false;
        boolean parsingSituation = false;
        boolean parsingDay = false;
        boolean parsingPrecipitation = false;
        Days tmpDay;
        DayForecast parsedDay = null;
        // the forecast is always in <pre></pre>, so find this part
        while ((line = reader.readLine()) != null) {
            if (! parsing) {
                if (line.contains("<pre>")) {
                    // start parsing
                    parsing = true;
                }
            } else {
                if (line.contains("</pre>")) {
                    // end parsing
                    break;
                } else {
                    // remove HTML tags,
                    // it could also be done using "Html.fromHtml(line).toString();", but that is much slower
                    line = cleanHTML(line);
                    // get rid of useless separators, such as ---- and ===
                    line = line.replaceAll("==+", "").replaceAll("--+", "").replace("\n","");
                    // skip empty lines after the cleanup
                    if (line.length() == 0) continue;
                    // the lines containing "ČHMÚ" are always purely informative and can be skipped
                    if (line.contains("ČHMÚ")) continue;
                    // also skip the time of release
                    if (line.contains("Čas vydání")) continue;

                    // try to find heading that says for which days the forecast is
                    // after the situation, there is always some forecast, that should be matched by matchDayForecast()
                    // it is likely that if we match a day name when parsing situation, the name just occured
                    // in the situation itself
                    if (! parsingSituation && (tmpDayEnum = matchDayHeader(line)).size() != 0) {
                        // store the data for the previous day and remove the day from the previousDayEnum
                        addDay(forecast, parsedDay, builderSituation, builderForecast, previousDayEnum);

                        // set the enum for the new list of days
                        previousDayEnum = tmpDayEnum;

                        // reset everything
                        parsingSituation = false;
                        parsingDay = false;
                        parsingPrecipitation = false;
                        parsedDay = null;
                        builderSituation.setLength(0);
                        builderForecast.setLength(0);
                    }
                    else {
                        // skip the informative lines telling the location (case insensitive)
                        if (line.matches(".*[Pp][Řř][Ee][Dd][Pp][Oo][Vv][Ěě][Ďď].*[Pp][Rr][Oo].*[Kk][Rr][Aa][Jj].*")) continue;
                        // find the situation
                        if (line.matches(".*[Ss]ituace\\s*:.*")) {
                            parsingSituation = true;
                            parsingDay = false;
                            // reset situation builder
                            builderSituation.setLength(0);
                        }
                        // special case: "Předpokládané množství srážek" used only for Prague
                        else if (line.contains("Předpokládané množství srážek")) {
                            parsingSituation = false;
                            parsingDay = false;
                            parsingPrecipitation = true;
                        }
                        // find the day forecast itself
                        else if ((tmpDay = matchDayForecast(line)) != null) {
                            parsingSituation = false;
                            parsingDay = true;
                            parsingPrecipitation = false;

                            // store the data for the previous day and remove the day from the previousDayEnum
                            addDay(forecast, parsedDay, builderSituation, builderForecast, previousDayEnum);

                            // set the forecast object for the new day
                            // if the new day is unknown, try to match with the previousDayEnum
                            if (tmpDay == Days.UNKNOWN && previousDayEnum != null && ! previousDayEnum.isEmpty()) {
                                parsedDay = new DayForecast(previousDayEnum.get(0));
                            } else {
                                parsedDay = new DayForecast(tmpDay);
                            }

                            // reset forecast builder
                            builderForecast.setLength(0);
                        }
                        // parse the data
                        else {
                            if (parsingSituation) {
                                builderSituation.append(' ');
                                builderSituation.append(line);
                            }
                            else if (parsingDay) {
                                builderForecast.append(' ');
                                builderForecast.append(line);
                            }
                            else if (parsingPrecipitation) {
                                tmpDay = matchDay(line.toLowerCase(LOCALE));
                                if (tmpDay != null) {
                                    // parse the amount of precipitation
                                    int colonPos = line.indexOf(':');
                                    if (colonPos > 0)
                                        precipitation.put(tmpDay, line.substring(colonPos));
                                }
                            }
                        }
                    }
                }
            }
        }

        // store the data for the last parsed day
        addDay(forecast, parsedDay, builderSituation, builderForecast, previousDayEnum);

        // add precipitation if it's not empty
        if (! precipitation.isEmpty()) {
            for (DayForecast it : forecast) {
                String dayPrecipitation = precipitation.get(it.getMatchingDay());
                if (dayPrecipitation != null && it.getForecast() != null) {
                    it.setForecast(it.getForecast().concat(" Srážky" + dayPrecipitation));
                }
            }
        }

        // add date information to the forecast
        fillDates(forecast);

        // cleanup the things that should not be part of the forecast
        cleanup(forecast);

        return forecast;
    }

    /**
     * A very simple method to clean HTML tags from line.
     *
     * NOTE: Only "<br />" tags are removed, because there are no other tags present
     * that could affect the parser.
     *
     * @param str string containing HTML tags to clean.
     * @return string stripped of HTML tags
     */
    private static String cleanHTML(String str) {
        return str.replace("<br />", "");
    }

    /**
     * Add a day to the forecast and remove it from the list of found days in previousDayEnum.
     *
     * @param day day to add to the forecast
     * @param builderSituation situation
     * @param builderForecast  forecast
     * @param previousDayEnum  list with found days
     */
    private static void addDay(ArrayList<DayForecast> forecast, DayForecast day, StringBuilder builderSituation, StringBuilder builderForecast, ArrayList<Days> previousDayEnum) {
        String tmpStr = builderForecast.toString().trim();
        if (day != null && tmpStr.length() != 0) {
            day.setSituation(builderSituation.toString().trim().replaceAll("\\s+", " "));
            day.setForecast(tmpStr.replaceAll("\\s+", " "));
            forecast.add(day);
            if (previousDayEnum != null) {
                // remove the matched day
                previousDayEnum.remove(day.getMatchingDay());
            }
        }
    }

    private static ArrayList<Days> matchDayHeader(String line) {
        ArrayList<Days> matchingDays = new ArrayList<>();

        // the lines ending with : are never the ones in heading but the ones that contains actual forecast
        if (line.endsWith(":")) {
            return matchingDays;
        }

        int startPos = 0;
        int endPos;
        while (startPos < line.length()) {
            endPos = line.indexOf(" ", startPos);
            if (endPos < 0) endPos = line.length();

            if (startPos == endPos) {
                startPos = endPos + 1;
                continue;
            }
            String word = line.substring(startPos, endPos);

            // try to match the word against a day
            if (word.contains("pondělí")) {
                matchingDays.add(Days.Mon);
            }
            else if (word.contains("úterý")) {
                matchingDays.add(Days.Tue);
            }
            else if (word.matches("střed[yau].*")) {
                matchingDays.add(Days.Wed);
            }
            else if (word.contains("čtvrtek")) {
                matchingDays.add(Days.Thu);
            }
            else if (word.contains("pátek")) {
                matchingDays.add(Days.Fri);
            }
            else if (word.matches("sobot[yau].*")) {
                matchingDays.add(Days.Sat);
            }
            else if (word.matches("neděl[ei].*")) {
                matchingDays.add(Days.Sun);
            }
            else if (word.matches("noc.*")) {
                matchingDays.add(Days.NIGHT);
            }

            // advance
            startPos = endPos + 1;
        }

        // if the line matches only night, remove it
        // that is because night often figures in forecast itself but in header it always
        // occurs together with a day name (eg. "Předpověď na noc a pondělí")
        boolean onlyNight = true;
        for (Days day : matchingDays) {
            if (day != Days.NIGHT) {
                onlyNight = false;
                break;
            }
        }
        if (onlyNight) {
            matchingDays.clear();
        }

        return matchingDays;
    }
    private static Days matchDayForecast(String line) {
        if (! line.endsWith(":")) {
            return null;
        }

        return matchDay(line);
    }

    private static Days matchDay(String line) {
        Days day = null;

        // day name matches with a specific day
        if (line.contains("pondělí")) {
            day = Days.Mon;
        }
        else if (line.contains("úterý")) {
            day = Days.Tue;
        }
        else if (line.matches(".*střed[yau].*")) {
            day = Days.Wed;
        }
        else if (line.contains("čtvrtek")) {
            day = Days.Thu;
        }
        else if (line.contains("pátek")) {
            day = Days.Fri;
        }
        else if (line.matches(".*sobot[yau].*")) {
            day = Days.Sat;
        }
        else if (line.matches(".*neděl[ei].*")) {
            day = Days.Sun;
        }

        // try to match "noc" with "noc"
        else if (line.contains("noc")) {
            day = Days.NIGHT;
        }

        // fallback - unknown day, will be matched by order
        else if (line.matches(".*([Pp]očasí|[Pp]ředpověď|den|večer).*")) {
            day = Days.UNKNOWN;
        }

        return day;
    }

    // adds a date to each DayForecast object in the forecast
    private static void fillDates(ArrayList<DayForecast> forecast) {
        // get the current date in Czech Republic
        Calendar calendar = Calendar.getInstance(TIME_ZONE);
        // set the time to a second after midnight today
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 1);

        // process known days
        for (DayForecast dayForecast : forecast) {
            // if the day of the forecast is known, find the closest corresponding date
            if (dayForecast.getMatchingDay() != Days.UNKNOWN &&
                dayForecast.getMatchingDay() != Days.NIGHT)
            {
                // find the closest corresponding date
                while (dayForecast.getMatchingDay().getCalendarDay() != calendar.get(Calendar.DAY_OF_WEEK)) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                }
                // set the date to the closest date
                dayForecast.setDate(calendar.getTime());
            }
        }

        // fill in date for NIGHT, the time of midnight is used
        // the filling is based on the next day, because the forecast for night should
        // never be last (to save some cycles), also there is only one forecast for night,
        // so break immediately after
        for (int i = 0; i < forecast.size(); i++) {
            DayForecast nightForecast = forecast.get(i);
            if (nightForecast.getMatchingDay() == Days.NIGHT) {
                // fill based on the next day
                if (i < forecast.size() - 1) {
                    nightForecast.setDate(forecast.get(i+1).getDate());
                }
                break;
            }
        }
    }

    /**
     * Cleans up entries that should not be part of the forecast
     * @param forecast forecast to clean up
     */
    private static void cleanup(ArrayList<DayForecast> forecast) {
        // remove the current weather that is sometimes part of the morning forecast in
        // Karlovarský and Plzeňský region
        if(forecast.size() > 2
           && forecast.get(0).getMatchingDay() == forecast.get(1).getMatchingDay()
           && forecast.get(0).getSituation().isEmpty()) {
            forecast.remove(0);
        }
    }
}

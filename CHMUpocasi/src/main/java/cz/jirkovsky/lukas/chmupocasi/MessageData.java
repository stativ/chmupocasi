/*
 *   Copyright 2014-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi;

import cz.jirkovsky.lukas.chmupocasi.forecast.data.Forecast;
import cz.jirkovsky.lukas.chmupocasi.forecast.data.Region;

/**
 * A class storing data of messages passed between the ForecastDownloader and MainActivity.
 */
public class MessageData {
    public enum TYPE {
        PROGRESS,
        SUCCESS,
        SUCCESS_UNCHANGED,
        ERROR
    }

    private final Region region;
    private final TYPE type;
    private final Forecast forecast;

    public MessageData(Region region, TYPE type) {
        this.region = region;
        this.type = type;
        this.forecast = null;
    }

    /**
     * Constructor for message with a valid forecast.
     *
     * @param region a region of the forecast
     * @param forecast the forecast for the region
     */
    public MessageData(Region region, Forecast forecast) {
        this.region = region;
        this.type = TYPE.SUCCESS;
        this.forecast = forecast;
    }

    public Region getRegion() {
        return region;
    }

    public TYPE getType() {
        return type;
    }

    /**
     * Get the stored forecast.
     *
     * The forecast is valid only if getType() == SUCCESS
     */
    public Forecast getForecast() {
        return forecast;
    }
}

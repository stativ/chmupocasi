/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.data;

import java.io.Serializable;

/**
 * A feature vector for the weather forecast.
 */
public class WeatherFeatures implements Serializable {
    private double features[];

    public enum WeatherFeature {
        // TODO: it would be nice to handle sun separately from the cloud cower in case that it is sunny in some places but cloudy elsewhere
        //SUN,
        CLOUD_COVER,
        RAIN,
        SHOWERS,
        STORM,
        FOG,
        SNOW
    }

    /**
     * Constructs feature vector for the weather forecast.
     */
    public WeatherFeatures(double features[]) {
        this.features = features;
    }

    public double getCloudCover() {
        return features[WeatherFeature.CLOUD_COVER.ordinal()];
    }

    public double getRain() {
        return features[WeatherFeature.RAIN.ordinal()];
    }

    public double getShowers() {
        return features[WeatherFeature.SHOWERS.ordinal()];
    }

    public double getStorm() {
        return features[WeatherFeature.STORM.ordinal()];
    }

    public double getSnow() {
        return features[WeatherFeature.SNOW.ordinal()];
    }
}

/*
 *   Copyright 2013-2017 Lukas Jirkovsky
 *
 *   This file is part of Počasí v krajích.
 *
 *   Počasí v krajích is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   Počasí v krajích is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Počasí v krajích.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.jirkovsky.lukas.chmupocasi.forecast.data;

import java.io.Serializable;
import java.util.Date;

import cz.jirkovsky.lukas.chmupocasi.forecast.parsing.WeatherFeaturesParser;

/**
 * Represents forecast for one day.
 *
 * The forecast may also include situation, but it is not mandatory.
 */
public class DayForecast implements Serializable {
    private final Days matchingDay;
    private Date date;
    private String situation;
    private String forecast;
    private WeatherFeatures features;

    public DayForecast(Days matchingDay) {
        this.matchingDay = matchingDay;
        features = null;
    }

    public Days getMatchingDay() {
        return matchingDay;
    }

    /** Get a date corresponding to the forecast.
     *
     * @return null if the matching day is not known or if it is night
     */
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation.trim();
    }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast.trim();
    }

    public synchronized WeatherFeatures getFeatures() {
        // lazy initialization
        if (features == null) {
            features = WeatherFeaturesParser.parse(forecast);
        }
        return features;
    }
}

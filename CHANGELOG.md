2.0.2
=====
* Nové adaptivní ikony pro spouštěč, které by měly fungovat lépe s novými verzemi Androidu.

2.0.1
=====
* Oprava stahování předpovědi.
* Aktualizace pro novější verze Androidu.
* Schování aktuálního počasí, které je občas součástí předpovědi v Plzeňském a Karlovarském kraji.
  Jeho zobrazení vedlo k zobrazení dvou záznamů pro dnešek. Jeho užitečnost byla navíc diskutabilní
  protože se týkala typicky počasí v 5 ráno.

2.0
===
* zobrazení času, kdy byla předpověď publikována na webu (na konci předpovědi vpravo dole)
* oprava nefunkční aktualizace, pokud byla předpověď nedávno aktualizována
* oprava pádu na zařízeních s Android 2.1 Eclair a Android 2.2 Froyo

2.0 rc4
=======
* oprava stahování předpovědi způsobená změnou na serverech ČHMÚ
* drobné opravy

2.0 rc3
=======
* drobná vylepšení zpracování předpovědi pro generování ikon
* oprava vypínání/zapínání ikon na Androidu 2.1 a 2.2

2.0 rc2
=======
* oprava možného pádu při zpracování předpovědi (#5)
* pročištění kódu

2.0 rc1
=======
* ikony už nejsou experimentální
* neblokující rozhraní - dialogy nahrazeny stavovou řádkou
* zlepšení zpracování textu předpovědi pro generování ikon
* zpracování předpokládaného množství srážek pro Prahu
* mazání starých přepovědí
* mírné vylepšení ikony spouštěče
* oprava podpory zařízení s velkým displayem na některých verzích Androidu
* explicitní povolení zařízení bez dotykové obrazovky (např. s d-padem)

2.0 beta2
=========
* další zlepšení zpracování textu předpovědi pro generování ikon
* oprava chyby, kdy v neděli mohlo být aktuální počasí označeno jako předpověď na pondělí

2.0 beta1
=========
* výrazné zlepšení zpracování textu předpovědi pro generování ikon
* nově ikonka sněhu
* oprava zvýrazňování teplot

1.3.1
=====
* optimalizace
* oprava pádu
* oprava odsazení textu okolo ikony při použití nestandardní velikosti písma

1.3.0
=====
* přepsání všech dialogů, takže se chovají jednotněji
* efektivnější stahování
* úpravy rozhraní,aby lépe pracovalo s prázdným místem, předpověď a situace se nově zobrazují na stejném místě a jde mezi nimi přepínat klikem na nadpis, šipku, nebo tažením
* vyhlazování ikon s počasím
* zlepšení přístupnosti na zařízeních s D-padem
* zvýrazňování teplot v předpovědi
